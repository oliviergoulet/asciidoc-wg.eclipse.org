---
block_type: "signup"
_build:
  render: never
---

image::../static/images/icons/chat.svg[opts=inline]

Join the +
Chat

https://chat.asciidoc.org[Join,role=button btn btn-primary]

image::../static/images/icons/email.svg[opts=inline]

Sign up to our +
Mailing list

https://accounts.eclipse.org/mailing-list/asciidoc-wg[Sign up,role=button btn btn-primary]
